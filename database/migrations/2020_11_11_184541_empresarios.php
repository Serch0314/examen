<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Empresarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresarios', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('codigo', 100);
            $table->text('razonsocial');
            $table->text('nombre');
            $table->text('pais');
            $table->text('tipo_moneda');
            $table->text('estado');
            $table->text('ciudad');
            $table->text('telefono');
            $table->text('correo');
            $table->boolean('activo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
