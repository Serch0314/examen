<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Empresario;
use Illuminate\Support\Facades\DB;
use SoapClient;
use SoapHeader;
class HomeController extends Controller
{

    public $validacionMoneda;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->validacionMoneda = null;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $total = Empresario::where('activo', 1)->count();
        if ($total > 0) {
            $empresarios = Empresario::where('activo', 1)->get();
        } else {
            $empresarios = null;
        }
        return view('home', compact('total', 'empresarios'));
    }

    public function abriFormulario() {
        return view('formulario');
    }

    public function crear()
    {
        return json_encode(array(
            'status' => true,
            'msg' => 'Home controller - crear'
        ));
        $cont = request()-> validate([
            'codigo'=> 'required',
            'razonsocial'=> 'required',
            'nombre'=> 'required',
            'pais'=> 'required',
            'tipo_moneda' => 'required',
            'estado'=> 'required',
            'ciudad'=> 'required',
            'telefono'=> 'required',
            'correo'=> 'required',
            'activo' => ''
        ]);

        $cont = request()->all();
        $cont['activo'] = true;

        $soapClient = new SoapClient("http://fx.currencysystem.com/webservices/CurrencyServer4.asmx?WSDL");
        $ap_param = array(
            'licenseKey' => '',
            'currency' => $cont['tipo_moneda']
        );
        $info = $soapClient->__call("CurrencyExists", array($ap_param));
        $this->validacionMoneda = $info->CurrencyExistsResult;

        if ( $this->validacionMoneda == false || $this->validacionMoneda == null ) {
            return json_encode(array('estatus' => false, 'mensaje' => 'moneda invalidad'));
        } else {
            if(Empresario::where('codigo', $cont['codigo'])->count() > 0) {
                return redirect()->action([HomeController::class, 'index']);
            }else {
                Empresario::create($cont);
                return redirect()->action([HomeController::class, 'index']);
            }

        }


    }

    public function validar( $moneda = null ) {
        if ( $moneda != null ) {
            $soapClient = new SoapClient("http://fx.currencysystem.com/webservices/CurrencyServer4.asmx?WSDL");
            $ap_param = array(
                'licenseKey' => '',
                'currency' => $moneda
            );
            $info = $soapClient->__call("CurrencyExists", array($ap_param));
            $this->validacionMoneda = $info->CurrencyExistsResult;
        }else {
            return view('formulario');
        }
    }

    public function eliminar($id)
    {
        $cont = Empresario::find($id);
        $cont->delete();
        return redirect()->action([HomeController::class, 'index']);
    }


}
