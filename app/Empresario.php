<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresario extends Model
{
    public $table = "empresarios";
    public $timestamps = false;

    protected $guarded = [];
}
