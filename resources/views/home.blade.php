@extends('layouts.app')

@section('content')
<div class="container">
    <div style="display: flex; justify-content: space-between; align-items: center; margin-bottom: 10px;">
        <h2 style="text-align: center;">Lista de empresarios</h2>
        <a href="{{ route('home.formulario') }}"  class="btn btn-primary">Agregar formulario</a>
    </div>
    @include('modal-insertar')
    <div>
        <table class="table">
            <thead>
              <tr>
                <th scope="col">Codigo</th>
                <th scope="col">Razón social</th>
                <th scope="col">Nombre</th>
                <th scope="col">Pais</th>
                <th scope="col">Tipo Moneda</th>
                <th scope="col">Estado</th>
                <th scope="col">Ciudad</th>
                <th scope="col">Telefono</th>
                <th scope="col">Correo</th>
                <th scope="col">Acciones</th>
              </tr>
            </thead>
            <tbody>
                @if( $empresarios != null)
                    @foreach ($empresarios as $empresario)
                        <input hidden type="number" value="{{ $empresario->codigo }}">
                        <tr>
                            <td>{{ $empresario->codigo }}</td>
                            <td>{{ $empresario->razonsocial }}</td>
                            <td>{{ $empresario->nombre }}</td>
                            <td>{{ $empresario->pais }}</td>
                            <td>{{ $empresario->tipo_moneda }}</td>
                            <td>{{ $empresario->estado }}</td>
                            <td>{{ $empresario->ciudad }}</td>
                            <td>{{ $empresario->telefono }}</td>
                            <td>{{ $empresario->correo }}</td>
                            <td>
                                <div style="display: flex; justify-content: center; align-items: center;">
                                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bd-example-modal-lg{{$empresario->id}}">
                                        <i class="fa fa-pencil" aria-hidden="true"  ></i>
                                    </button>
                                    <button type="button" class="btn btn-danger" onclick="eliminar({{$empresario->id}});"><i class="fa fa-trash" aria-hidden="true" ></i></button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
          </table>
          @if($total == 0)
          <div class="alert alert-warning" role="alert">
            No hay datos
          </div>
          @else
          <div style="display: flex; justify-content: center; align-items: center;">
            <small>Total de empresarios: <strong>{{ $total }}</strong></small>
          </div>
          @endif
    </div>
</div>
<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script>
    function eliminar(id) {
            var url = '{{ route("home.eliminar", ":id") }}';
            url = url.replace(':id', id);
            window.location.href = url;
    }
</script>
@endsection
