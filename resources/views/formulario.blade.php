@extends('layouts.app')

@section('content')
<form id="form_empresario" action="{{ route('home.crear') }}"  method="POST">
    @csrf
    <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="basic-addon1">Codigo</span>
        </div>
    <input type="text" class="form-control" placeholder="codigo..." aria-label="Username" aria-describedby="basic-addon1" name="codigo" value="{{ old('codigo') }}">
    </div>

    <small style="color: red;">{{ $errors->first('codigo') }}</small>

    <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="basic-addon1">Razón social</span>
        </div>
        <input type="text" class="form-control" placeholder="Razón social..." aria-label="Username" aria-describedby="basic-addon1" name="razonsocial" value="{{ old('razonsocial') }}">
    </div>
    <small style="color: red;">{{ $errors->first('razonsocial') }}</small>

    <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="basic-addon1">Nombre</span>
        </div>
        <input type="text" class="form-control" placeholder="nombre" aria-label="Username" aria-describedby="basic-addon1" name="nombre" value="{{ old('nombre') }}">
    </div>
    <small style="color: red;">{{ $errors->first('nombre') }}</small>


    <div class="form-row">
        <div class="col">

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Codigo moneda</span>
                </div>
                <input type="text" id="tipo_moneda" class="form-control" placeholder="codigo modena..." aria-label="Username" aria-describedby="basic-addon1" name="tipo_moneda" value="{{ old('tipo_moneda') }}">
            </div>
            <small id="error_tipo_moneda" style="color: red;">{{ $errors->first('tipo_moneda') }}</small>
        </div>
        <div class="col">
            <div class="input-group mb-3">
                <button type="button" class="btn btn-warning btn-block" onclick="verificar()">Verificar</button>
            </div>
        </div>
    </div>

    <div class="form-row">
        <div class="col">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Pais</span>
                </div>
                <input type="text" class="form-control" placeholder="Pais..." aria-label="Username" aria-describedby="basic-addon1" name="pais" value="{{ old('pais') }}">
            </div>
            <small style="color: red;">{{ $errors->first('pais') }}</small>
        </div>
        <div class="col">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Estado</span>
                </div>
                <input type="text" class="form-control" placeholder="Estado..." aria-label="Username" aria-describedby="basic-addon1" name="estado" value="{{ old('estado') }}">
            </div>
            <small style="color: red;">{{ $errors->first('estado') }}</small>
        </div>
    </div>

    <div class="form-row">
        <div class="col">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Ciudad</span>
                </div>
                <input type="text" class="form-control" placeholder="Ciudad..." aria-label="Username" aria-describedby="basic-addon1" name="ciudad" value="{{ old('ciudad') }}">
            </div>
            <small style="color: red;">{{ $errors->first('ciudad') }}</small>
        </div>
        <div class="col">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Telefono</span>
                </div>
                <input type="text" class="form-control" placeholder="Telefono..." aria-label="Username" aria-describedby="basic-addon1" name="telefono" value="{{ old('telefono') }}">
            </div>
            <small style="color: red;">{{ $errors->first('telefono') }}</small>
        </div>
    </div>

    <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="basic-addon1">Correo</span>
        </div>
        <input type="text" class="form-control" placeholder="Correo..." aria-label="Username" aria-describedby="basic-addon1" name="correo" value="{{ old('correo') }}">
    </div>
    <small style="color: red;">{{ $errors->first('correo') }}</small>

    <button type="button" id="btn_formulario" class="btn btn-primary">Guardar</button>
</form>
<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script>
    function verificar() {
    var moneda = document.getElementById('tipo_moneda').value;
    console.log(moneda);
    }

    $(document).ready(function(){

        $('body').on('click','#btn_formulario',function (e) {
            $.ajax({
                headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                type : "POST",
                url : '{{route('home.crear')}}',
                data : {},
                dataType : "json",
                success:function (response) {
                    console.log(response);
                    $('#error_tipo_moneda').html('errores: '+response.msg);
                },
                error: function (xhr) {
                    alert(xhr.status);
                }
            });
        });

    });

</script>

@endsection
