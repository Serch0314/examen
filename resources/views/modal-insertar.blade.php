<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Agregar empresario</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        

    <form action="{{ route('home.crear') }}"  method="POST">
        @csrf
            <div class="modal-body">

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1">Codigo</span>
                    </div>
                <input type="text" class="form-control" placeholder="codigo..." aria-label="Username" aria-describedby="basic-addon1" name="codigo" value="{{ old('codigo') }}">
                </div>

                <small style="color: red;">{{ $errors->first('codigo') }}</small>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1">Razón social</span>
                    </div>
                    <input type="text" class="form-control" placeholder="Razón social..." aria-label="Username" aria-describedby="basic-addon1" name="razonsocial" value="{{ old('razonsocial') }}">
                </div>
                <small style="color: red;">{{ $errors->first('razonsocial') }}</small>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1">Nombre</span>
                    </div>
                    <input type="text" class="form-control" placeholder="nombre" aria-label="Username" aria-describedby="basic-addon1" name="nombre" value="{{ old('nombre') }}">
                </div>
                <small style="color: red;">{{ $errors->first('nombre') }}</small>

                <div class="form-row">
                    <div class="col">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="basic-addon1">Codigo moneda</span>
                            </div>
                            <input type="text" class="form-control" placeholder="codigo modena..." aria-label="Username" aria-describedby="basic-addon1" name="tipo_moneda" value="{{ old('tipo_moneda') }}">
                        </div>
                        <small style="color: red;">{{ $errors->first('tipo_moneda') }}</small>
                    </div>
                    <div class="col">
                        <div class="input-group mb-3">
                            <button type="button" class="btn btn-warning btn-lg btn-block">Validar</button>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="basic-addon1">Pais</span>
                            </div>
                            <input type="text" class="form-control" placeholder="Pais..." aria-label="Username" aria-describedby="basic-addon1" name="pais" value="{{ old('pais') }}">
                        </div>
                        <small style="color: red;">{{ $errors->first('pais') }}</small>
                    </div>
                    <div class="col">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="basic-addon1">Estado</span>
                            </div>
                            <input type="text" class="form-control" placeholder="Estado..." aria-label="Username" aria-describedby="basic-addon1" name="estado" value="{{ old('estado') }}">
                        </div>
                        <small style="color: red;">{{ $errors->first('estado') }}</small>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="basic-addon1">Ciudad</span>
                            </div>
                            <input type="text" class="form-control" placeholder="Ciudad..." aria-label="Username" aria-describedby="basic-addon1" name="ciudad" value="{{ old('ciudad') }}">
                        </div>
                        <small style="color: red;">{{ $errors->first('ciudad') }}</small>
                    </div>
                    <div class="col">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="basic-addon1">Telefono</span>
                            </div>
                            <input type="text" class="form-control" placeholder="Telefono..." aria-label="Username" aria-describedby="basic-addon1" name="telefono" value="{{ old('telefono') }}">
                        </div>
                        <small style="color: red;">{{ $errors->first('telefono') }}</small>
                    </div>
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1">Correo</span>
                    </div>
                    <input type="text" class="form-control" placeholder="Correo..." aria-label="Username" aria-describedby="basic-addon1" name="correo" value="{{ old('correo') }}">
                </div>
                <small style="color: red;">{{ $errors->first('correo') }}</small>

                
            </div>

              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              </div>

            </div>
        </form>
        
    </div>
</div>  