<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/formulario', 'HomeController@abriFormulario')->name('home.formulario');
Route::post('/home/crear', [ HomeController::class, 'crear' ])->name('home.crear');
Route::get('/home/validar/{moneda?}', [ HomeController::class, 'validar' ])->name('home.validar');
Route::get('/home/eliminar/{id}', [ HomeController::class, 'eliminar' ])->name('home.eliminar');
Route::post('/home/editar', [ HomeController::class, 'editar' ])->name('home.editar');